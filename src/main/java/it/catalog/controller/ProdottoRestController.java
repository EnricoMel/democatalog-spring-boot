package it.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.catalog.model.Prodotto;
import it.catalog.repository.ProdottoRepository;

@RestController
@RequestMapping("/prodottorest")
public class ProdottoRestController {
	
	@Autowired
	ProdottoRepository service;
	
	@GetMapping("/prodotti")
	public Iterable<Prodotto> getProdotti() {
		return service.findAll();
	}
	
	@GetMapping("/prodotto/{id}")
	public Prodotto getProdotto(@PathVariable int id) {
		return service.findById(id).get();
	}
	
	@PostMapping("/add")
	public Prodotto add(@RequestBody Prodotto p) {
		return service.save(p);
	}
}
