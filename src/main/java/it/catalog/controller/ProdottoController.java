package it.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import it.catalog.model.Prodotto;
import it.catalog.repository.ProdottoRepository;

@Controller
@RequestMapping("/prodotto")
public class ProdottoController {
	
	@Autowired
	private ProdottoRepository repository;
	
	/*@GetMapping("/elenco")
	public String list() {
		
		Iterable<Prodotto> products = repository.findAll();
		products.forEach((Prodotto p) -> {
			System.out.println(p.getNome());
		});
		
		return "prodotti";
	}*/
	
	@GetMapping("/elenco")
	public ModelAndView list() {
		Iterable<Prodotto> products = repository.findAll();
		return new ModelAndView("prodotti", "listaProdotti", products);
	}
	
	
	
}
