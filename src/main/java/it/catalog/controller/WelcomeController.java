package it.catalog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WelcomeController {
	
	@ResponseBody
	@GetMapping("/test")
	public String test() {
		return "Sono nel controller!";
	}
	
	@GetMapping("/")
	public String index() {
		return "index";
	}

}
