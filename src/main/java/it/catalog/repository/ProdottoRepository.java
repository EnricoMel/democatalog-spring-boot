package it.catalog.repository;

import org.springframework.data.repository.CrudRepository;

import it.catalog.model.Prodotto;

public interface ProdottoRepository extends CrudRepository<Prodotto, Integer>{
	
}	
