package it.catalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCatalogBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoCatalogBootApplication.class, args);
	}

}
